# Entertaining configs

Those are programs configuration files or bash scripts of mine.

Now the repository contains the following configs:

* [Emacs](https://gitlab.com/qualimock/entertaining-configs/-/tree/main/Emacs). 
	* Remove old *.emacs.d* directory and *.emacs* config, then
	```bash
	$ cp -r emacs.d $HOME/.emacs.d
	$ cp emacsconf $HOME/.emacs
	```

* [Project Initializers](https://gitlab.com/qualimock/entertaining-configs/-/tree/main/project_initializers):
    * Cpp:
        * *init_cmake.sh* generates an empty CMake project
        * *init_meson.sh* generates an empty Meson project
    * Qt (testing): there is only "init_qt.sh" bash script which generates an empty CMake Qt project

* [Zsh](https://gitlab.com/qualimock/entertaining-configs/-/tree/main/zsh):
    * *zshrc*. Main config for zsh
    * *zsh* directory. All other config and useful files
    * *qmock.zsh-theme*. Theme for [oh-my-zsh](https://ohmyz.sh/)
	```bash
	$ cp zshrc $HOME/.zshrc
	$ cp -r zsh $HOME/.config
	```
