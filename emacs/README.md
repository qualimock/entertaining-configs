# Emacs config

## Keybindings

### Movement

| Binding |          Description          |
|---------|-------------------------------|
|  `M-j`  | Move left                     |
|  `M-k`  | Move down                     |
|  `M-l`  | Move up                       |
|  `M-;`  | Move right                    |
|  `M-J`  | Move back a word              |
|  `M-K`  | Move to the beginning of line |
|  `M-L`  | Move to the end of line       |
|  `M-;`  | Move forward a word           |
|  `M-[`  | Move backward a paragraph     |
|  `M-]`  | Move forward a paragraph      |

### Editing

| Binding   | Description                        |
|-----------|------------------------------------|
| `C-z`     | Undo                               |
| `C-g C-z` | Redo                               |
| `M-o`     | Insert newline before current line |
| `C-o`     | Insert newline after current line  |
| `C-c /`   | Comment a region                   |
| `C-c r`   | Replace with regexp                |
| `C-y y`   | Copy line                          |
| `C-y Y`   | Duplicate line                     |
| `C-y C-y` | Copy region                        |
| `C-y p`   | Paste                              |
| `C-d d`   | Delete region                      |
| `C-d C-d` | Delete line                        |

### Window actions

| Binding | Description                          |
|---------|--------------------------------------|
| `C-w v` | Split window to left and right parts |
| `C-w s` | Split window to top and bottom parts |
| `C-w n` | Open buffer in a new frame           |
| `C-J`   | Move cursor to left window           |
| `C-K`   | Move cursor to bottom window         |
| `C-L`   | Move cursor to top window            |
| `C-:`   | Move cursor to right window          |
