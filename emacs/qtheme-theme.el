(deftheme qtheme
  "Qualimock Is The Creator")

(custom-theme-set-faces
 'qtheme
 '(fixed-pitch ((t (:family "Hack"))))
 '(header-line ((t (:box nil :inverse-video nil))))
 '(minibuffer-prompt ((t (:background "gray93" :foreground "red3" :box nil :weight bold))))
 '(font-lock-type-face ((t (:foreground "#0084C8" :bold t))))
 '(font-lock-builtin-face ((t (:foreground "#0084C8"))))
 '(font-lock-constant-face ((t (:foreground "#0084C8"))))
 '(font-lock-comment-face ((t (:foreground "red3"))))
 '(font-lock-function-name-face ((t (:foreground "#00578E" :bold t))))
 '(font-lock-keyword-face ((t (:bold t :foreground "#A52A2A"))))
 '(font-lock-string-face ((t (:foreground "#4E9A06"))))
 '(font-lock-variable-name-face ((t (:foreground "#0084C8" :bold t))))
 '(font-lock-warning-face ((t (:foreground "red3" :bold t))))
 '(default ((t (:inherit nil :extend t :stipple nil :background "gray93" :foreground "#2E3436" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight regular :height 133 :width normal :foundry "SRC" :family "Hack"))))
 '(highlight ((t (:background "powder blue"))))
 '(company-tooltip ((t (:background "gainsboro" :foreground "black"))))
 '(company-tooltip-scrollbar-track ((t (:background "light gray"))))
 '(region ((t (:extend t :background "powder blue"))))
 '(show-paren-match ((t (:background "powder blue"))))
 '(widget-button ((t (:bold t :foreground "#0084C8"))))
 '(ido-subdir ((t (:inherit font-lock-constant-face))))
 '(mode-line ((t (:background "gray80" :foreground "#2E3436" :height 116))))
 '(mode-line-inactive ((t (:inherit mode-line :background "gray93" :foreground "#2E3436" :weight light))))
 '(telephone-line-accent-active ((t (:background "gray93" :foreground "#2E3436"))))
 '(telephone-line-accent-inactive ((t (:background "gray80" :foreground "#2E3436"))))
 '(telephone-line-projectile ((t (:inherit mode-line :foreground "red3" :weight bold))))
 '(dired-directory ((t (:inherit font-lock-keyword-face)))))

(provide-theme 'qtheme)
