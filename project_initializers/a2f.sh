#!/bin/bash

if [ $# -lt 2 ]; then
	echo "Each argument is a pattern for $1"
	echo "No arguments provided"
	exit
fi

for arg in ${@:2}; do
	echo $arg >> $1
done
