#!/bin/bash

if [ "" == "$1" ]; then
	echo "No project name provided"
	exit
fi

if [ -d $1 ]; then
   echo "Project $1 already exists"
   exit
fi

mkdir $1 && cd $1 && mkdir src
echo "Create $1 dir"
echo "Create $1/src dir"

echo "#include <iostream>

int main(int argc, char **argv)
{
	return 0;
}" >> src/main.cpp
echo "Create src/main.cpp"

echo "cmake_minimum_required(VERSION 3.16 FATAL_ERROR)
project($1)

set(CMAKE_CXX_STANDARD 17)

add_executable(\${PROJECT_NAME}
  src/main.cpp)

set_target_properties(\${PROJECT_NAME} PROPERTIES RUNTIME_OUTPUT_DIRECTORY \${CMAKE_BINARY_DIR}/bin)
" >> CMakeLists.txt
echo "Create CMakeLists.txt"

echo "BUILDDIR = $1_build

all: configure build

configure:
	cmake -H. -B \$(BUILDDIR) -DCMAKE_BUILD_TYPE=Debug -DCMAKE_EXPORT_COMPILE_COMMANDS=YES .

build:
	cmake --build \$(BUILDDIR)

clean:
	rm -rf \$(BUILDDIR)

run:
	\$(BUILDDIR)/bin/$1 $args

debug:
	gdb \$(BUILDDIR)/bin/$1 $args
" >> Makefile
echo "Create Makefile"

make

ln -s $(pwd)/builddir/compile_commands.json

echo "Created new project with name '$1'"
echo "You can build and run it with command"
echo "make all run"
