#!/bin/sh

if [ "" == "$1" ]; then
	echo "No project name provided"
	exit
fi

if [ -d $1 ]; then
   echo "Directory $1 already exists"
   exit
fi

mkdir $1 && cd $1 && mkdir src
echo "[D] $1"
echo "[D] $1/src"

echo
echo "[F] src/main.cpp"
echo "#include <iostream>

int main(int argc, char **argv)
{
	return 0;
}" | tee -a src/main.cpp

echo
echo "[F] meson.build"
echo "project('$1', ['cpp', 'c'], version: '0.1.0', meson_version: '>= 0.59.0')

subdir('src')" | tee -a meson.build

echo
echo "[F] src/meson.build"
echo "$1_sources = [
  'main.cpp',
]

$1_deps = []

executable('$1', $1_sources,
  dependencies: $1_deps,
  install: true,
)" | tee -a src/meson.build

echo
echo "[P] meson setup build"
meson setup build

echo "[P] ln -s $(pwd)/build/compile_commands.json"
ln -s $(pwd)/build/compile_commands.json

echo "[P] touch .projectile"
touch .projectile

echo "[P] Initialized project '$1'"
