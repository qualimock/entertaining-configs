#!/bin/bash

if [ "" == "$1" ]; then
	echo "No project name provided"
	exit
fi

if [ -d $1 ]; then
   echo "Project $1 already exists"
   exit
fi

mkdir $1 && cd $1 && mkdir src
echo "Create $1 dir"
echo "Create $1/src dir"

echo "#include \"mainwindow.h\"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}" >> src/main.cpp
echo "Create src/main.cpp"

echo "#include \"mainwindow.h\"
#include \"./ui_mainwindow.h\"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}" >> src/mainwindow.cpp
echo "Create src/mainwindow.cpp"

echo "#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H" >> src/mainwindow.h
echo "Create src/mainwindow.h"

echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<ui version=\"4.0\">
 <class>MainWindow</class>
 <widget class=\"QMainWindow\" name=\"MainWindow\">
  <property name=\"geometry\">
   <rect>
    <x>0</x>
    <y>0</y>
    <width>800</width>
    <height>600</height>
   </rect>
  </property>
  <property name=\"windowTitle\">
   <string>MainWindow</string>
  </property>
  <widget class=\"QWidget\" name=\"centralwidget\"/>
  <widget class=\"QMenuBar\" name=\"menubar\">
   <property name=\"geometry\">
    <rect>
     <x>0</x>
     <y>0</y>
     <width>800</width>
     <height>20</height>
    </rect>
   </property>
  </widget>
  <widget class=\"QStatusBar\" name=\"statusbar\"/>
 </widget>
 <resources/>
 <connections/>
</ui>" >> src/mainwindow.ui
echo "Create src/mainwindow.ui"

echo "cmake_minimum_required(VERSION 3.16 FATAL_ERROR)
project($1 VERSION 1.0.0 LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(QT NAMES Qt6 Qt5 REQUIRED COMPONENTS Widgets)
find_package(Qt\${QT_VERSION_MAJOR} REQUIRED COMPONENTS Widgets)

set(PROJECT_SOURCES
	src/main.cpp
	src/mainwindow.cpp
	src/mainwindow.h
	src/mainwindow.ui
)

if(\${QT_VERSION_MAJOR} GREATER_EQUAL 6)
    qt_add_executable(\${PROJECT_NAME}
		MANUAL_FINALIZATION
		\${PROJECT_SOURCES}
    )
else()
    if(ANDROID)
        add_library(\${PROJECT_NAME} SHARED
            \${PROJECT_SOURCES}
        )
    else()
        add_executable(\${PROJECT_NAME}
            \${PROJECT_SOURCES}
        )
    endif()
endif()

target_link_libraries(\${PROJECT_NAME} PRIVATE Qt\${QT_VERSION_MAJOR}::Widgets)

set_target_properties(\${PROJECT_NAME} PROPERTIES
  RUNTIME_OUTPUT_DIRECTORY \${CMAKE_BINARY_DIR}/bin
  WIN32_EXECUTABLE ON
  MACOSX_BUNDLE ON
)

if(QT_VERSION_MAJOR EQUAL 6)
    qt_finalize_executable(\${PROJECT_NAME})
endif()" >> CMakeLists.txt
echo "Create CMakeLists.txt"

echo "builddir = $1_build

all:
	cmake -H. -B \$(builddir) -DCMAKE_BUILD_TYPE=Debug -DCMAKE_EXPORT_COMPILE_COMMANDS=YES . && cmake --build \$(builddir)

remake:
	cmake -H. -B \$(builddir) -DCMAKE_BUILD_TYPE=Debug -DCMAKE_EXPORT_COMPILE_COMMANDS=YES .

clean:
	rm -rf build

run:
	\$(builddir)/bin/$1

debug:
	gdb \$(builddir)/bin/$1
" >> Makefile
echo "Create Makefile"

make

ln -s $(pwd)/$1_build/compile_commands.json
ln -s $(pwd)/$1_build/$1_autogen/include/ui_mainwindow.h src/

echo "Created new project with name '$1'"
echo "You can build and run it with command"
echo "make all run"
