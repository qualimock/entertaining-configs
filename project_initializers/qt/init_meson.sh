#!/bin/bash

if [ "" == "$1" ]; then
	echo "No project name provided"
	exit
fi

if [ -d $1 ]; then
   echo "Directory $1 already exists"
   exit
fi

mkdir $1 && cd $1 && mkdir src
echo "[D] $1"
echo "[D] $1/src"

echo
echo "[F] src/main.cpp:"
echo "#include \"mainwindow.h\"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}" | tee -a src/main.cpp

echo
echo "[F] src/mainwindow.cpp"
echo "#include \"mainwindow.h\"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent) {
}

MainWindow::~MainWindow() {
}" | tee -a src/mainwindow.cpp

echo
echo "[F] src/mainwindow.h"
echo "#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
};

#endif // MAINWINDOW_H" | tee -a src/mainwindow.h

echo
echo "[F] meson.build"
echo "project('$1', 'cpp')

subdir('src')" | tee -a meson.build

echo
echo "[F] src/meson.build"
echo "qt6 = import('qt6')

qt6_dep = dependency('qt6', modules: ['Core', 'Gui', 'Widgets'])

moc_files = qt6.compile_moc(headers : 'mainwindow.h',
                            dependencies: qt6_dep)

executable('$1', 'main.cpp', 'mainwindow.cpp', moc_files,
           dependencies: qt6_dep)" | tee -a src/meson.build

echo "[P] meson setup build"
meson setup build

echo "[P] ln -s $(pwd)/build/compile_commands.json"
ln -s $(pwd)/build/compile_commands.json

echo "[P] touch .projectile"
touch .projectile

echo "[P] Created new project with name '$1'"
