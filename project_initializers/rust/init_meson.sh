#!/bin/bash

if [ "" == "$1" ]; then
	echo "No project name provided"
	exit
fi

if [ -d $1 ]; then
   echo "Project $1 already exists"
   exit
fi

cargo init $1

cd $1

echo "project('$1', 'rust',
          version: '0.1.0',
    meson_version: '>= 0.59.0',
  default_options: [ 'warning_level=2', 'werror=false', 'cpp_std=gnu++2a', ],
)

subdir('src')" >> meson.build
echo "Create meson.build"

echo "$1_sources = [
  'main.rs',
]

$1_deps = [
]

executable('$1', $1_sources,
  dependencies: $1_deps,
  install: true,
)" >> src/meson.build
echo "Create src/meson.build"

meson setup builddir

echo "Initialized project '$1'"
