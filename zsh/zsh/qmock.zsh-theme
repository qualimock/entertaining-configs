setopt promptsubst
PR="%n@%B%F{green}%m%f%b: %T %B%F{green}%~%f%b $ "

set_custom_prompt() {
	if [[ $(git config user.email | awk -F@ '{print $2}') == "altlinux.org" ]]; then
		PROMPT="ALT $PR"
	else
		PROMPT=$PR
	fi
}

typeset -a precmd_functions
precmd_functions+=(set_custom_prompt)
